"""
This plugin will fill a list of feeds in the style of <ul>...</ul>

In your template use the tag __feedlist__ to have replaced with all the configured feeds.

Version 0.1
Daniel Molkentin
"""
import rawdoglib.plugins
import rawdoglib.rawdog
import cgi

def links_output_bits(rawdog, config, bits):
    links = "<ul>\n"

    feeds = [(feed.get_html_name(config).lower(), feed)
             for feed in rawdog.feeds.values()]
    feeds.sort()

    for (key, feed) in feeds:
        links += '\t<li><a class="xmlbutton" href="' + cgi.escape(feed.url) + '"> '
        links += '<img src="images/icon-rss.png" title="Feed source" alt="Feed source" /></a>' + feed.get_html_link(config) + '</li>\n'
    links += "</ul>\n"
    bits['feedlist'] = links

rawdoglib.plugins.attach_hook('output_bits', links_output_bits)
